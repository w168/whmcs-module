<?php

use WHMCS\Database\Capsule;

function webnode_conf_config()
{
	return [
		'name' => 'Webnode Sitebuilder Configuration',
		'version' => '0.0.3',
		'author' => 'webnode',
		'language' => 'english',
		'fields' => [
			'oauth_uri' => [
				"FriendlyName" 	=> "OAuth Url",
				"Type" 			=> "text",
				"Size" 			=> "80",
				"Description" 	=> "Oauth API Base Url"
			],
			'api_url' => [
				"FriendlyName" 	=> "Api Url",
				"Type" 			=> "text",
				"Size" 			=> "80",
				"Description" 	=> "API-Gateway Base Url"
			],
			'client_id' => [
				"FriendlyName" 	=> "Client Id",
				"Type" 			=> "text",
				"Size" 			=> "80",
				"Description" 	=> ""
			],
			'client_secret' => [
				"FriendlyName" 	=> "Client Secret",
				"Type" 			=> "password",
				"Size" 			=> "80",
				"Description" 	=> ""
			],
		],
	];
}

function webnode_conf_activate()
{
	$schema = Capsule::schema();

	$projectsCreated = false;
	$queueCreated = false;


	try
	{
		if (!$schema->hasTable('mod_webnode_project')) {
			$projectsCreated = true;
			$schema->create(
				'mod_webnode_project',
				function ($table) {
					$table->increments('id'); // Table ID
					$table->integer('serviceId')->unique(); // Product ID on whmcs side
					$table->string('projectIdentifier', 256);
					$table->string('adminUserIdentifier', 256);
				}
			);
		}

		if (!$schema->hasTable('mod_webnode_queue')) {
			$queueCreated = true;
			$schema->create(
				'mod_webnode_queue',
				function ($table) {
					$table->increments('id'); // Table ID
					$table->string('projectIdentifier', 256);
					$table->string('action', 256);
					$table->text('params');
				}
			);
		}
	}
	catch (\Throwable $e)
	{
		return [
			// Supported values here include: success, error or info
			'status' => "error",
			'description' => 'Unable to create webnode tables: ' . $e->getMessage(),
		];
	}

	return [
		'status' => 'success',
		'description' => 'Module active. Table mod_webnode_project '
			. ($projectsCreated ? 'created' : 'already existed')
			. '. mod_webnode_queue '
			. ($queueCreated ? 'created' : 'already existed'),
	];
}

function webnode_conf_output(array $vars)
{
	$oauthUri = empty($vars['oauth_uri']) ? '<span style="font-size: bold; color: red">UNSET</span>' : $vars['oauth_uri'];
	$apiUrl = empty($vars['api_url']) ? '<span style="font-size: bold; color: red">UNSET</span>' : $vars['api_url'];
	$clientId = empty($vars['client_id']) ? '<span style="font-size: bold; color: red">UNSET</span>' : $vars['client_id'];
	$clientSecret = empty($vars['client_secret']) ? '<span style="font-size: bold; color: red">UNSET</span>' : '....................';

	echo "
		<hr/>
		<h2>Current settings</h2>
		<p>OAuth Url<pre>$oauthUri</pre></p>
		<p>API Url<pre>$apiUrl</pre></p>
		<p>Client Id<pre>$clientId</pre></p>
		<p>Client Secret<pre>$clientSecret</pre></p>
	";
}
