<?php

namespace wnd\whmcs\unit\integration;

use wnd\whmcs\models\Project\Project;

class FacadeReadTest extends AbstractApiIntegration
{

	public static function setUpBeforeClass(): void
	{
		self::staticSetup();
	}


	public function testFindProject(): void
	{
		$project = self::$webnodeFacade->findByIdentifier(self::$config['testData']['projects']['basic']['identifier']);
		$this->assertNotNull($project);
	}

	public function testFindProjects(): void
	{
		$projects = self::$webnodeFacade->findByUserIdentifier(self::$config['testData']['user']['hash']);
		$this->assertNotNull($projects);

		foreach (self::$config['testData']['projects'] as $testData)
		{
			$this->assertTrue($this->isProjectInList($projects, $testData['identifier']));
		}
	}

	/**
	 * @param Project[] $list
	 * @param string $identifier
	 * @return bool
	 */
	public function isProjectInList(array $list, string $identifier): bool
	{
		foreach ($list as $project)
		{
			if ($project->getIdentifier() === $identifier)
			{
				return true;
			}
		}
		return false;
	}
}
