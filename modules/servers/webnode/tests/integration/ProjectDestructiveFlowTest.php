<?php

namespace wnd\whmcs\unit\integration;

use wnd\whmcs\models\Project\Project;

/**
 * If anything in those tests fails, it can ruin the project it's testing
 */
class ProjectDestructiveFlowTest extends AbstractApiIntegration
{

	/** @var string */
	private static $userEmail;

	/** @var string */
	private static $identifier;

	public static function setUpBeforeClass(): void
	{
		self::staticSetup();
	}


	public function testCreateProject(): void
	{
		self::$identifier = $this->getUniqueName();
		self::$userEmail = self::$identifier . '@rubicus.cz';

		$project = self::$webnodeFacade->create(self::$userEmail, self::$identifier);
		self::$webnodeFacade->waitForProjectReady($project->getProjectIdentifier(), 10000, 1000);
		$this->assertTrue(is_string($project->toString()));
	}

	/**
	 * @depends testCreateProject
	 */
	public function testDeleteProject(): void
	{
		self::$webnodeFacade->deleteProject(self::$identifier);
		$project = self::$webnodeFacade->findByIdentifier(self::$identifier);
		$this->assertNotNull($project);
		$this->assertEquals('DELETED', $project->getStatus());
	}

	/**
	 * @depends testDeleteProject
	 */
	public function testRestoreProject(): void
	{
		self::$webnodeFacade->restoreSoftDeletedProject(self::$identifier);

		$project = $this->waitFor(
			function () {
				return self::$webnodeFacade->findByIdentifier(self::$identifier);
			},
			function (Project $project) {
				return $project->getStatus() !== 'DELETED';
			}
		);

		$this->assertNotNull($project);
		$this->assertNotEquals('DELETED', $project->getStatus());
	}

	/**
	 * @depends testRestoreProject
	 */
	public function testEnableDisableProject(): void
	{
		self::$webnodeFacade->setProjectEnabled(self::$identifier, false);
		$project = self::$webnodeFacade->findByIdentifier(self::$identifier);
		$this->assertNotNull($project);
		$this->assertEquals('DISABLED', $project->getStatus());

		self::$webnodeFacade->setProjectEnabled(self::$identifier, true);
		$project = self::$webnodeFacade->findByIdentifier(self::$identifier);
		$this->assertNotNull($project);
		$this->assertNotEquals('DISABLED', $project->getStatus());
	}
}
