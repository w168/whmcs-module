<h3> {$LANG.sitebuilder.name|default:"Sitebuilder"} </h3>
<div class="row">
	<div class="col-sm-5 text-right">
		<strong>{$LANG.sitebuilder.ipName|default:"Project IPs"}</strong>
	</div>
	<div class="col-sm-7 text-left">
		{foreach $ips as $ip}
			{$ip}<br>
		{/foreach}
	</div>
</div>
<h4>{$LANG.sitebuilder.header|default:"Required Configuration"}</h4>
<div class="row">
	<table class="table">
		<thead>
			<tr>
				<th>{$LANG.sitebuilder.table.type|default:"Type"}</th>
				<th>{$LANG.sitebuilder.table.name|default:"Name"}</th>
				<th>{$LANG.sitebuilder.table.value|default:"Value"}</th>
			</tr>
		</thead>
		<tbody class="text-left">
			{foreach $ips as $ip}
			<tr>
				<td>A</td>
				<td>{$domain}</td>
				<td>
					{$ip}
				</td>
			</tr>
			{/foreach}
			<tr>
				<td>CNAME</td>
				<td>*.{$domain}</td>
				<td>
					{$cname}
				</td>
			</tr>
		</tbody>
	</table>
	{if not $domainAssigned }
		<p style="font-weight: bold;">{$LANG.sitebuilder.domainNotAssignedText|default:"Domain is not set to project. Please setup DNS records above and assign domain."}</p>
		<a href="clientarea.php?action=productdetails&amp;id={$serviceId}&amp;userAction=assignDomain" class="btn btn-primary">{$LANG.sitebuilder.domainNotAssignedButton|default:"Assign Domain"}</a>
	{elseif not $httpsActive }
		<p style="font-weight: bold;">{$LANG.sitebuilder.httpsNotActiveText|default:"Domain has not active HTTPS. Please setup DNS records above and refresh certificate."}</p>
		<a href="clientarea.php?action=productdetails&amp;id={$serviceId}&amp;userAction=refreshCert" class="btn btn-primary">{$LANG.sitebuilder.httpsNotActiveButton|default:"Refresh certificate"}</a>
    {/if}
</div>

<h4>{$LANG.sitebuilder.cmsHeader|default:"Go to sitebuilder edit:"}</h4>
<p class="text-center">
	<a href="clientarea.php?action=productdetails&amp;id={$serviceId}&amp;userAction=ssoEdit" class="btn btn-primary">{$LANG.sitebuilder.cmsButton|default:"Edit"}</a>
</p>

<span
	style="display: none"
	data-reset-url="clientarea.php?action=productdetails&amp;id={$serviceId}&amp;userAction=reset&amp;projectIdentifier={$projectIdentifier}"
></span>
