<div class="alert alert-danger text-center">
    {$LANG.sitebuilder.inactiveText|default:"Project is being prepared, please visit later"}
</div>

<p class="text-center">
	<a onclick="location.reload()" class="btn btn-primary">{$LANG.sitebuilder.inactiveButton|default:"Refresh"}</a>
</p>
