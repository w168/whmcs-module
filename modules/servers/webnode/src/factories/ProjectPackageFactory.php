<?php

declare(strict_types=1);

namespace wnd\whmcs\factories;

use wnd\whmcs\exceptions\InvalidDataException;
use wnd\whmcs\models\ProjectPackage\ProjectPackage;

final class ProjectPackageFactory
{
	/**
	 * @param array<int|string, mixed> $data
	 * @throws InvalidDataException
	 */
	public function create(array $data): ProjectPackage
	{
		try
		{
			return new ProjectPackage($data['identifier'] ?? null, $data['isActive'] ?? null);
		}
		catch (\TypeError $e)
		{
			throw new InvalidDataException('Invalid project package data received from an API: ' . $e->getMessage());
		}
	}
}
