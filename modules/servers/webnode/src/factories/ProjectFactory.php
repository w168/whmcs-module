<?php

declare(strict_types=1);

namespace wnd\whmcs\factories;

use wnd\whmcs\exceptions\InvalidDataException;
use wnd\whmcs\models\Project\Limits;
use wnd\whmcs\models\Project\Project;

final class ProjectFactory
{
	/**
	 * @param array<int|string, mixed> $data
	 * @throws InvalidDataException
	 */
	public function create(array $data): Project
	{
		try
		{
			$limits = new Limits(
				$data['limits']['bandwidthMaximum'] ?? null,
				$data['limits']['bandwidthUsage'] ?? null,
				$data['limits']['storageMaximum'] ?? null,
				$data['limits']['storageUsage'] ?? null
			);

			return new Project(
				$data['identifier'] ?? null,
				$data['projectUrl'] ?? null,
				$data['domainName'] ?? null,
				$data['cmsUrl'] ?? null,
				$data['adminUserIdentifier'] ?? null,
				$data['portalUrl'] ?? null,
				$data['serverIpAddresses'] ?? null,
				$data['cname'] ?? null,
				$data['status'] ?? null,
				$data['replyToEmail'] ?? null,
				$data['adminEmail'] ?? null,
				$data['installationCompleted'] ?? null,
				$limits
			);
		}
		catch (\TypeError $e)
		{
			throw new InvalidDataException('Invalid project data supplied: ' . $e->getMessage());
		}
	}
}
