<?php

declare(strict_types=1);

namespace wnd\whmcs\factories;

use wnd\whmcs\exceptions\InvalidDataException;
use wnd\whmcs\models\ProjectDomain\ProjectDomain;

final class ProjectDomainFactory
{
	/**
	 * @param array<int|string, mixed> $data
	 * @throws InvalidDataException
	 */
	public function create(array $data): ProjectDomain
	{
		try
		{
			return new ProjectDomain(
				$data['domainName'] ?? null,
				$data['createdDate'] ?? null,
				$data['projectIdentifier'] ?? null,
				$data['userIdentifier'] ?? null,
				$data['isHttps'] ?? null,
				$data['isMasterDomain'] ?? null
			);
		}
		catch (\TypeError $e)
		{
			throw new InvalidDataException('Invalid project domain data received from an API: ' . $e->getMessage());
		}
	}
}
