<?php

namespace wnd\whmcs\factories;

use wnd\whmcs\ApiClient\AuthorizationService;
use wnd\whmcs\ApiClient\ProjectCreateClient;
use wnd\whmcs\ApiClient\ProjectReadClient;
use wnd\whmcs\ApiClient\ProjectUpdateClient;
use wnd\whmcs\ApiClient\AuthorizedClient;
use wnd\whmcs\ApiClient\WebnodeOAuth2Client;
use wnd\whmcs\HttpClient\Client;
use wnd\whmcs\WebnodeFacade;

class WebnodeFacadeFactory
{
	/** @var bool  */
	private $verify;

	public function __construct(bool $verify = true)
	{
		$this->verify = $verify;
	}

	private function createOAuth2Api(string $public, string $secret, string $oAuth2Url):WebnodeOAuth2Client
	{
		$client = new Client($oAuth2Url, $this->verify);
		return new WebnodeOAuth2Client($public, $secret, $client);
	}

	private function createAuthorizedApiClient(AuthorizationService $authorizationService, string $apiUrl): AuthorizedClient
	{
		$client = new Client($apiUrl, $this->verify);
		return new AuthorizedClient($authorizationService, $client);
	}


	public function createFacade(string $public, string $secret, string $oAuth2Url, string $apiUrl): WebnodeFacade
	{
		$oAuth2Api = $this->createOAuth2Api($public, $secret, $oAuth2Url);
		$authorizeApiService = new AuthorizationService($oAuth2Api);
		$api = $this->createAuthorizedApiClient($authorizeApiService, $apiUrl);

		$reader = new ProjectReadClient(
			$api,
			new ProjectFactory(),
			new ProjectPackageFactory(),
			new ProjectDomainFactory()
		);

		return new WebnodeFacade(
			$reader,
			new ProjectCreateClient($api, $reader),
			new ProjectUpdateClient($api, $reader),
			$oAuth2Api,
			$authorizeApiService
		);
	}
}
