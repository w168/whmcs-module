<?php

namespace wnd\whmcs\HttpClient;

class Response
{
	/**
	 * Hold response raw body.
	 *
	 * @var string
	 */
	private $body;

	/**
	 * Hold response code
	 *
	 * @var int
	 */
	private $statusCode;

	/**
	 * Hold response raw headers.
	 *
	 * @var array<string, string>|null
	 */
	private $headers;

	public function __construct(string $body, int $statusCode, string $headers)
	{
		$this->body = $body;
		$this->statusCode = $statusCode;
		$this->processHeaders($headers);
	}

	private function processHeaders(string $headersStr): void
	{
		$headers = array();
		$arrRequests = explode("\r\n\r\n", $headersStr);
		for ($index = 0; $index < count($arrRequests) - 1; $index++)
		{
			foreach (explode("\r\n", $arrRequests[$index]) as $i => $line)
			{
				if ($i === 0)
				{
					$headers[$index]['http_code'] = $line;
				}
				else
				{
					list($key, $value) = explode(': ', $line);
					$headers[$index][$key] = $value;
				}
			}
		}
		$this->headers = $headers[0] ?? null;
	}

	public function getStatusCode(): int
	{
		return $this->statusCode;
	}

	/**
	 * @return array<string, string>|null
	 */
	public function getHeaders(): ?array
	{
		return $this->headers;
	}

	/**
	 * Get response header's specific key's value
	 *
	 * @param string $key
	 * @return string|null
	 */
	public function getHeader(string $key): ?string
	{
		$headers = $this->getHeaders();
		if (is_array($headers))
		{
			return $headers[$key];
		}
		else
		{
			return null;
		}
	}

	/**
	 * Get response raw body
	 */
	public function getBody(): string
	{
		return $this->body;
	}
}
