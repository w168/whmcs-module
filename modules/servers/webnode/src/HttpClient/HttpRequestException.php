<?php

namespace wnd\whmcs\HttpClient;

use wnd\whmcs\exceptions\WebnodeBaseException;

class HttpRequestException extends WebnodeBaseException
{

}
