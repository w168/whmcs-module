<?php

declare(strict_types=1);

namespace wnd\whmcs\models\Project;

final class Limits
{
	/**
	 * @var ?float
	 */
	private $bandwidthMaximum;

	/**
	 * @var ?float
	 */
	private $bandwidthUsage;

	/**
	 * @var ?int
	 */
	private $storageMaximum;

	/**
	 * @var ?int
	 */
	private $storageUsage;

	public function __construct(
		?float $bandwidthMaximum,
		?float $bandwidthUsage,
		?int $storageMaximum,
		?int $storageUsage
	) {
		$this->bandwidthMaximum = $bandwidthMaximum;
		$this->bandwidthUsage = $bandwidthUsage;
		$this->storageMaximum = $storageMaximum;
		$this->storageUsage = $storageUsage;
	}

	public function getBandwidthMaximum(): ?float
	{
		return $this->bandwidthMaximum;
	}

	public function getBandwidthUsage(): ?float
	{
		return $this->bandwidthUsage;
	}

	public function getStorageMaximum(): ?int
	{
		return $this->storageMaximum;
	}

	public function getStorageUsage(): ?int
	{
		return $this->storageUsage;
	}
}
