<?php

declare(strict_types=1);

namespace wnd\whmcs\models\Project;

final class Project
{
	/**
	 * @var string
	 */
	private $identifier;

	/**
	 * @var string
	 */
	private $projectUrl;

	/**
	 * @var string
	 */
	private $domainName;

	/**
	 * @var string
	 */
	private $cmsUrl;

	/**
	 * @var string
	 */
	private $adminUserIdentifier;

	/**
	 * @var string
	 */
	private $portalUrl;

	/**
	 * @var string[]
	 */
	private $serverIpAddresses;

	/**
	 * @var string
	 */
	private $cname;

	/**
	 * @var string
	 */
	private $status;

	/**
	 * @var string
	 */
	private $replyToEmail;

	/**
	 * @var string
	 */
	private $adminEmail;

	/**
	 * @var bool
	 */
	private $installationCompleted;

	/**
	 * @var Limits
	 */
	private $limits;

	/**
	 * @param string[] $serverIpAddresses
	 */
	public function __construct(
		string $identifier,
		string $projectUrl,
		string $domainName,
		string $cmsUrl,
		string $adminUserIdentifier,
		string $portalUrl,
		array $serverIpAddresses,
		string $cname,
		string $status,
		string $replyToEmail,
		string $adminEmail,
		bool $installationCompleted,
		Limits $limits
	) {

		$this->identifier = $identifier;
		$this->projectUrl = $projectUrl;
		$this->domainName = $domainName;
		$this->cmsUrl = $cmsUrl;
		$this->adminUserIdentifier = $adminUserIdentifier;
		$this->portalUrl = $portalUrl;
		$this->serverIpAddresses = $serverIpAddresses;
		$this->cname = $cname;
		$this->status = $status;
		$this->replyToEmail = $replyToEmail;
		$this->adminEmail = $adminEmail;
		$this->installationCompleted = $installationCompleted;
		$this->limits = $limits;
	}

	public function getIdentifier(): string
	{
		return $this->identifier;
	}

	public function getProjectUrl(): string
	{
		return $this->projectUrl;
	}

	public function getDomainName(): string
	{
		return $this->domainName;
	}

	public function getCmsUrl(): string
	{
		return $this->cmsUrl;
	}

	public function getAdminUserIdentifier(): string
	{
		return $this->adminUserIdentifier;
	}

	public function getPortalUrl(): string
	{
		return $this->portalUrl;
	}

	/**
	 * @return string[]
	 */
	public function getServerIpAddresses(): array
	{
		return $this->serverIpAddresses;
	}

	public function getCname(): string
	{
		return $this->cname;
	}

	public function getStatus(): string
	{
		return $this->status;
	}

	public function getReplyToEmail(): string
	{
		return $this->replyToEmail;
	}

	public function getAdminEmail(): string
	{
		return $this->adminEmail;
	}

	public function isInstallationCompleted(): bool
	{
		return $this->installationCompleted;
	}

	public function getLimits(): Limits
	{
		return $this->limits;
	}
}
