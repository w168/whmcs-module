<?php

declare(strict_types=1);

namespace wnd\whmcs\models\ProjectDomain;

final class ProjectDomain
{
	/**
	 * @var string
	 */
	private $domainName;

	/**
	 * @var ?string
	 */
	private $createdDate;

	/**
	 * @var string
	 */
	private $projectIdentifier;

	/**
	 * @var string
	 */
	private $userIdentifier;

	/**
	 * @var bool
	 */
	private $isHttps;

	/**
	 * @var bool
	 */
	private $isMasterDomain;

	public function __construct(
		string $domainName,
		?string $createdDate,
		string $projectIdentifier,
		string $userIdentifier,
		bool $isHttps,
		bool $isMasterDomain
	) {
		$this->domainName = $domainName;
		$this->createdDate = $createdDate;
		$this->projectIdentifier = $projectIdentifier;
		$this->userIdentifier = $userIdentifier;
		$this->isHttps = $isHttps;
		$this->isMasterDomain = $isMasterDomain;
	}

	public function getDomainName(): string
	{
		return $this->domainName;
	}

	public function getCreatedDate(): ?string
	{
		return $this->createdDate;
	}

	public function getProjectIdentifier(): string
	{
		return $this->projectIdentifier;
	}

	public function getUserIdentifier(): string
	{
		return $this->userIdentifier;
	}

	public function isHttps(): bool
	{
		return $this->isHttps;
	}

	public function isMasterDomain(): bool
	{
		return $this->isMasterDomain;
	}
}
