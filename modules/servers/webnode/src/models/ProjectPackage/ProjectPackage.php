<?php

declare(strict_types=1);

namespace wnd\whmcs\models\ProjectPackage;

final class ProjectPackage
{
	/**
	 * @var string
	 */
	private $identifier;

	/**
	 * @var bool
	 */
	private $isActive;

	public function __construct(string $identifier, bool $isActive)
	{

		$this->identifier = $identifier;
		$this->isActive = $isActive;
	}

	public function getIdentifier(): string
	{
		return $this->identifier;
	}

	public function isActive(): bool
	{
		return $this->isActive;
	}
}
