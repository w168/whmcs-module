<?php

declare(strict_types=1);

namespace wnd\whmcs;

use wnd\whmcs\exceptions\InvalidDataException;
use wnd\whmcs\exceptions\ModuleIsNotConfiguredException;
use wnd\whmcs\exceptions\ProjectNotFoundException;
use wnd\whmcs\factories\WebnodeFacadeFactory;
use wnd\whmcs\models\Project\Project;

final class WebnodeProvisioningModule
{
	/**
	 * @var WhmcsRepository
	 */
	private $whmcsRepository;

	/**
	 * @var WebnodeFacade
	 */
	private $facade;

	/**
	 * If settings are not provided they are fetched from the WHMCS repository (should be configured using webnode_conf
	 * addon)
	 *
	 * @param ApiSettings|null $apiSettings
	 * @throws ModuleIsNotConfiguredException
	 */
	public function __construct(?ApiSettings $apiSettings = null)
	{
		$this->whmcsRepository = new WhmcsRepository();

		$settings = $apiSettings ?? $this->whmcsRepository->getSettings();
		if (!$settings)
		{
			throw new ModuleIsNotConfiguredException('Webnode module is not configured. Use webnode_conf addon to do it');
		}

		$this->facade = (new WebnodeFacadeFactory())->createFacade(
			$settings->getClientId(),
			$settings->getClientSecret(),
			$settings->getOauthUri(),
			$settings->getApiUrl()
		);
	}

	/**
	 * @throws InvalidDataException
	 * @throws ProjectNotFoundException
	 */
	public function loadProject(int $whmcsServiceId): Project
	{
		$projectInfo = $this->whmcsRepository->getProjectInfo($whmcsServiceId);
		return $this->facade->findByIdentifier($projectInfo->projectIdentifier);
	}

	public function getWebnodeApiClient(): WebnodeFacade
	{
		return $this->facade;
	}

	public function getWhmcsRepository(): WhmcsRepository
	{
		return $this->whmcsRepository;
	}

	public function recoverProject(int $serviceId): bool
	{
		try
		{
			$projectInfo = $this->whmcsRepository->getProjectInfo($serviceId);
		}
		catch (ProjectNotFoundException $exception)
		{
			// service id not saved -> nothing to recover
			return false;
		}

		try
		{
			$project = $this->facade->findByIdentifier($projectInfo->projectIdentifier);
		}
		catch (ProjectNotFoundException $exception)
		{
			// project deleted permanently, remove from WHMCS database
			$this->whmcsRepository->deleteProject($serviceId);
			return false;
		}

		$this->getWebnodeApiClient()->restoreSoftDeletedProject($project->getIdentifier());
		return true;
	}

	public function createNewProject(
		int $serviceId,
		string $userEmail,
		string $package,
		string $language = 'en'
	): void {
		// create new
		$project = $this->getWebnodeApiClient()->create($userEmail, null, $language);
		$this->getWebnodeApiClient()->changePackage($project->getProjectIdentifier(), $package);
		$this->getWhmcsRepository()->addProject(
			$serviceId,
			$project->getProjectIdentifier(),
			$project->getUserHash()
		);

		$this->getWebnodeApiClient()->clean();
	}
}
