<?php

declare(strict_types=1);

namespace wnd\whmcs\exceptions;

/**
 * Exception is thrown when invalid data supplied.
 */
final class InvalidDataException extends WebnodeBaseException
{

}
