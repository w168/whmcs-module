<?php

declare(strict_types=1);

namespace wnd\whmcs\exceptions;

final class ModuleIsNotConfiguredException extends WebnodeBaseException
{

}
