<?php

namespace wnd\whmcs\ApiClient;

use wnd\whmcs\exceptions\InvalidDataException;
use wnd\whmcs\factories\ProjectDomainFactory;
use wnd\whmcs\factories\ProjectFactory;
use wnd\whmcs\factories\ProjectPackageFactory;
use wnd\whmcs\HttpClient\Response;
use wnd\whmcs\models\Project\Project;
use wnd\whmcs\models\ProjectDomain\ProjectDomain;
use wnd\whmcs\models\ProjectPackage\ProjectPackage;

class ProjectReadClient
{
	/** @var AuthorizedClient  */
	private $api;

	/**
	 * @var ProjectFactory
	 */
	private $projectFactory;

	/**
	 * @var ProjectPackageFactory
	 */
	private $projectPackageFactory;

	/**
	 * @var ProjectDomainFactory
	 */
	private $projectDomainFactory;

	public function __construct(
		AuthorizedClient $api,
		ProjectFactory $projectFactory,
		ProjectPackageFactory $projectPackageFactory,
		ProjectDomainFactory $projectDomainFactory
	) {
		$this->api = $api;
		$this->projectFactory = $projectFactory;
		$this->projectPackageFactory = $projectPackageFactory;
		$this->projectDomainFactory = $projectDomainFactory;
	}

	/**
	 * @return Project[]
	 * @throws InvalidDataException
	 */
	public function findByUserIdentifier(string $userIdentifier): array
	{
		$data = $this->getResponseData($this->api->request('GET', "/users/$userIdentifier/projects"));

		return array_map(function ($projectData) {
			return $this->projectFactory->create($projectData);
		}, $data ?? []);
	}

	/**
	 * @throws InvalidDataException
	 */
	public function findByIdentifier(string $identifier): ?Project
	{
		$data = $this->getResponseData($this->api->request('GET', "/projects/$identifier"));
		return $data === null ? null : $this->projectFactory->create($data);
	}

	/**
	 * @return ProjectPackage[]
	 * @throws InvalidDataException
	 */
	public function getProjectPackages(string $identifier): array
	{
		$data = $this->getResponseData($this->api->request('GET', "/projects/$identifier/packages"));

		return array_map(function (array $packageData) {
			return $this->projectPackageFactory->create($packageData);
		}, $data ?? []);
	}

	/**
	 * @return ProjectDomain[]
	 * @throws InvalidDataException
	 */
	public function getProjectDomains(string $identifier): array
	{
		$data = $this->getResponseData($this->api->request('GET', "/projects/$identifier/domains"));

		return array_map(function ($domainData) {
			return $this->projectDomainFactory->create($domainData);
		}, $data ?? []);
	}


	public function getCurrentProjectForDomain(string $domain): ?string
	{
		$data = $this->getResponseData($this->api->request('GET', "/domains/$domain"));
		if ($data)
		{
			return $data['projectIdentifier'];
		}
		return null;
	}

	/**
	 * @param Response $response
	 * @return array<string|int, mixed>|null
	 */
	private function getResponseData(Response $response): ?array
	{
		if ($response->getStatusCode() < 300)
		{
			return json_decode($response->getBody(), true)['data'];
		}
		return null;
	}
}
