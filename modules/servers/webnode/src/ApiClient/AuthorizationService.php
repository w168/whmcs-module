<?php

declare(strict_types=1);

namespace wnd\whmcs\ApiClient;

/**
 * Service for providing API authorization header
 */
final class AuthorizationService
{
	/** @var string|null  */
	private $token = null;

	/** @var WebnodeOAuth2Client */
	private $oAuth2Client;

	public function __construct(WebnodeOAuth2Client $oAuth2Client)
	{
		$this->oAuth2Client = $oAuth2Client;
	}


	public function getAuthorizationHeader(): string
	{
		$token = $this->getAccessToken();

		return "Bearer $token";
	}

	private function getAccessToken(): string
	{
		if (!$this->token)
		{
			$this->token = $this->oAuth2Client->clientCredentialsGrant();
		}

		return $this->token;
	}

	public function revokeAccessToken(): void
	{
		if ($this->token)
		{
			$this->oAuth2Client->revokeToken($this->token);
			$this->token = null;
		}
	}
}
