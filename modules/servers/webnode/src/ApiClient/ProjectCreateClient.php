<?php

namespace wnd\whmcs\ApiClient;

use Exception;
use InvalidArgumentException;
use wnd\whmcs\exceptions\InvalidDataException;
use wnd\whmcs\models\Project\Project;
use wnd\whmcs\exceptions\WebnodeBaseException;
use wnd\whmcs\NewProjectResponse;

class ProjectCreateClient
{
	/** @var AuthorizedClient */
	private $api;
	/** @var ProjectReadClient */
	private $readClient;

	public function __construct(AuthorizedClient $api, ProjectReadClient $readClient)
	{
		$this->api = $api;
		$this->readClient = $readClient;
	}

	/**
	 * @param string $userEmail
	 * @param string|null $identifier
	 * @return NewProjectResponse
	 * @throws InvalidArgumentException
	 * @throws Exception
	 *
	 */
	public function create(
		string $userEmail,
		string $identifier = null,
		string $language = 'en'
	): NewProjectResponse {
		$identifier = $identifier ?? 'w' . bin2hex(random_bytes(10));
		// call an API
		$response = $this->api->request('POST', '/projects', [
			'projectIdentifierHint' => $identifier,
			'language' => $language,
			'email' => $userEmail,
			'replyToEmail' => '',
		]);
		$responseData = json_decode($response->getBody(), true);

		return (new NewProjectResponse())
			->setUserHash($responseData['data']['adminUserIdentifier'])
			->setProjectIdentifier($identifier)
			->setRedirectUrl($responseData['data']['cmsUrl']);
	}

	/**
	 * @throws InvalidDataException
	 */
	public function isProjectReady(string $identifier, ?Project $preloadedProject = null): bool
	{
		if ($preloadedProject)
		{
			return $preloadedProject->isInstallationCompleted();
		}

		$project = $this->readClient->findByIdentifier($identifier);
		if (empty($project))
		{
			return false;
		}
		return $project->isInstallationCompleted();
	}

	/**
	 * @param string $identifier
	 * @param int $timeout (in ms)
	 * @param int $step (in ms)
	 * @return void
	 * @throws WebnodeBaseException
	 * @throws InvalidDataException
	 */
	public function waitForProjectReady(string $identifier, int $timeout = 60000, int $step = 250): void
	{
		$timeout *=1000;
		$step *=1000;

		$msLeft = $timeout;

		while (!$this->isProjectReady($identifier))
		{
			if ($msLeft <= 0)
			{
				throw new WebnodeBaseException('Project-ready wait timeout');
			}
			$msLeft -= $step;
			usleep($step);
		}
	}
}
