<?php

declare(strict_types=1);

namespace wnd\whmcs;

use Exception;
use InvalidArgumentException;
use wnd\whmcs\ApiClient\AuthorizationService;
use wnd\whmcs\ApiClient\ProjectCreateClient;
use wnd\whmcs\ApiClient\ProjectReadClient;
use wnd\whmcs\ApiClient\ProjectUpdateClient;
use wnd\whmcs\ApiClient\WebnodeOAuth2Client;
use wnd\whmcs\exceptions\InvalidDataException;
use wnd\whmcs\exceptions\ProjectNotFoundException;
use wnd\whmcs\models\Project\Project;
use wnd\whmcs\models\ProjectDomain\ProjectDomain;
use wnd\whmcs\models\ProjectPackage\ProjectPackage;
use wnd\whmcs\exceptions\WebnodeBaseException;

final class WebnodeFacade
{
	/** @var ProjectReadClient */
	private $readClient;

	/** @var ProjectCreateClient */
	private $projectCreateClient;

	/** @var ProjectUpdateClient */
	private $updateProjectClient;

	/** @var WebnodeOAuth2Client */
	private $oAuth2Api;

	/** @var AuthorizationService */
	private $authorizationService;

	public function __construct(
		ProjectReadClient $readClient,
		ProjectCreateClient $projectCreateClient,
		ProjectUpdateClient $updateProjectClient,
		WebnodeOAuth2Client $oAuth2Api,
		AuthorizationService $authorizationService
	) {
		$this->readClient = $readClient;
		$this->projectCreateClient = $projectCreateClient;
		$this->updateProjectClient = $updateProjectClient;
		$this->oAuth2Api = $oAuth2Api;
		$this->authorizationService = $authorizationService;
	}

	public function clean(): void
	{
		$this->authorizationService->revokeAccessToken();
	}

	/**
	 * @return Project[]
	 * @throws InvalidDataException
	 */
	public function findByUserIdentifier(string $userIdentifier): ?array
	{
		return $this->readClient->findByUserIdentifier($userIdentifier);
	}

	/**
	 * @throws InvalidDataException
	 * @throws ProjectNotFoundException
	 */
	public function findByIdentifier(string $identifier): Project
	{
		$project = $this->readClient->findByIdentifier($identifier);
		if ($project === null)
		{
			throw new ProjectNotFoundException("Project with identifier '$identifier' is not found");
		}

		return $project;
	}

	/**
	 * @throws InvalidDataException
	 * @return ProjectDomain[]
	 */
	public function getProjectDomains(string $identifier): array
	{
		return $this->readClient->getProjectDomains($identifier);
	}

	/**
	 * @throws InvalidDataException
	 * @return ProjectPackage[]
	 */
	public function getProjectPackages(string $identifier): array
	{
		return $this->readClient->getProjectPackages($identifier);
	}

	public function deleteProject(string $identifier): void
	{
		$this->updateProjectClient->deleteProject($identifier);
	}

	public function resetProject(string $identifier): void
	{
		$this->updateProjectClient->resetProject($identifier);
	}

	/**
	 * For 30 days after project deletion, you are able to restore it
	 * @param string $identifier
	 * @return void
	 * @throws InvalidDataException
	 */
	public function restoreSoftDeletedProject(string $identifier): void
	{
		$this->updateProjectClient->restoreSoftDeletedProject($identifier);
	}

	/**
	 * @throws WebnodeBaseException
	 * @throws InvalidDataException
	 */
	public function setProjectEnabled(string $identifier, bool $enabled): void
	{
		$this->updateProjectClient->setProjectEnabled($identifier, $enabled);
	}

	/**
	 * @throws Exception
	 */
	public function create(string $userEmail, string $identifier = null, string $language = 'en'): NewProjectResponse
	{
		return $this->projectCreateClient->create($userEmail, $identifier, $language);
	}

	/**
	 * @param string $identifier
	 * @param int $timeout (in ms)
	 * @param int $step (in ms)
	 * @return void
	 * @throws WebnodeBaseException
	 * @throws InvalidDataException
	 */
	public function waitForProjectReady(string $identifier, int $timeout = 60000, int $step = 250): void
	{
		$this->projectCreateClient->waitForProjectReady($identifier, $timeout, $step);
	}

	/**
	 * @throws InvalidDataException
	 */
	public function isProjectReady(string $identifier, ?Project $preloadedProjectData = null): bool
	{
		return $this->projectCreateClient->isProjectReady($identifier, $preloadedProjectData);
	}

	/**
	 * @throws InvalidDataException
	 */
	public function changePackage(string $identifier, ?string $newPackage): void
	{
		$this->updateProjectClient->changePackage($identifier, $newPackage);
	}

	/**
	 * @throws InvalidDataException
	 */
	public function setMasterDomainToProject(string $identifier, string $domain): bool
	{
		if (!$this->isProjectReady($identifier))
		{
			return false;
		}
		return $this->updateProjectClient->setMasterDomainToProject($identifier, $domain);
	}

	public function refreshCertificate(string $domain): bool
	{
		return $this->updateProjectClient->refreshCertificate($domain);
	}

	/**
	 * @param string $userIdentifier
	 * @param string $redirectUri (portal/cms url)
	 * @return string
	 */
	public function getSingleSignOn(string $userIdentifier, string $redirectUri): string
	{
		return $this->oAuth2Api->getAuthorizationUrlForUser($userIdentifier, $redirectUri);
	}
}
