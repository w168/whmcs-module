<?php

use wnd\whmcs\WebnodeProvisioningModule;

if (!defined("WHMCS"))
{
	die("This file cannot be accessed directly");
}

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related abilities and
 * settings.
 *
 * @see https://developers.whmcs.com/provisioning-modules/meta-data-params/
 *
 * @return array
 */
function webnode_MetaData()
{
	return array(
		'DisplayName' => 'Webnode Sitebuilder',
		'APIVersion' => '1.0',
		'RequiresServer' => false,
		'DefaultNonSSLPort' => '80',
		'DefaultSSLPort' => '443',
		'ServiceSingleSignOnLabel' => 'Login to web administration as User',
	);
}


/**
 * Define product configuration options.
 *
 * The values you return here define the configuration options that are
 * presented to a user when configuring a product for use with the module. These
 * values are then made available in all module function calls with the key name
 * configoptionX - with X being the index number of the field from 1 to 24.
 *
 * You can specify up to 24 parameters, with field types:
 * * text
 * * password
 * * yesno
 * * dropdown
 * * radio
 * * textarea
 *
 * Examples of each and their possible configuration parameters are provided in
 * this sample function.
 *
 * @see https://developers.whmcs.com/provisioning-modules/config-options/
 *
 * @return array
 */
function webnode_ConfigOptions()
{
	return [
		'package' => [
			'Type' => 'dropdown',
			'Options' => [
				'limited' => 'Limited',
				'mini' => 'Mini',
				'standard' => 'Standard',
				'professional' => 'Professional',
				'business' => 'Business',
			],
			'Default' => 'standard',
			'Description' => 'Package to install on a project',
			"FriendlyName" => 'Package Name',
		],
		'language' => [
			'Type' => 'dropdown',
			'Options' => [
				'en' => 'en',
				'pt' => 'pt',
				'es' => 'es',
				'it' => 'it',
				'fi' => 'fi',
			],
			'Default' => 'en',
			'Description' => 'Language of the frontend and administration',
			'FriendlyName' => 'Language',
		]
	];
}


/**
 * Provision a new instance of a product/service.
 *
 * Attempt to provision a new instance of a given product/service. This is
 * called any time provisioning is requested inside of WHMCS. Depending upon the
 * configuration, this can be any of:
 * * When a new order is placed
 * * When an invoice for a new order is paid
 * * Upon manual request by an admin user
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_CreateAccount(array $params)
{
	try
	{
		$module = new WebnodeProvisioningModule();
		// try to recover project
		if ($module->recoverProject($params['serviceid']))
		{
			return 'success';
		}
		// proejct not recovered (didn't exist or hard deleted) -> create new one
		$module->createNewProject(
			$params['serviceid'],
			$params['clientsdetails']['email'],
			$params['configoption1'],
			!empty($params['configoption2']) ? $params['configoption2'] : 'en'
		);
		return 'success';
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}
}

/**
 * Suspend an instance of a product/service.
 *
 * Called when a suspension is requested. This is invoked automatically by WHMCS
 * when a product becomes overdue on payment or can be called manually by admin
 * user.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_SuspendAccount(array $params)
{
	try
	{
		$module = new WebnodeProvisioningModule();
		$project = $module->loadProject($params['serviceid']);
		$module->getWebnodeApiClient()->setProjectEnabled($project->getIdentifier(), false);
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Un-suspend instance of a product/service.
 *
 * Called when an un-suspension is requested. This is invoked
 * automatically upon payment of an overdue invoice for a product, or
 * can be called manually by admin user.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_UnsuspendAccount(array $params)
{
	try
	{
		$module = new WebnodeProvisioningModule();
		$project = $module->loadProject($params['serviceid']);
		$module->getWebnodeApiClient()->setProjectEnabled($project->getIdentifier(), true);
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Terminate instance of a product/service.
 *
 * Called when a termination is requested. This can be invoked automatically for
 * overdue products if enabled, or requested manually by an admin user.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_TerminateAccount(array $params)
{
	try
	{
		$module = new WebnodeProvisioningModule();
		$project = $module->loadProject($params['serviceid']);
		$module->getWebnodeApiClient()->deleteProject($project->getIdentifier());
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Upgrade or downgrade an instance of a product/service.
 *
 * Called to apply any change in product assignment or parameters. It
 * is called to provision upgrade or downgrade orders, as well as being
 * able to be invoked manually by an admin user.
 *
 * This same function is called for upgrades and downgrades of both
 * products and configurable options.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_ChangePackage(array $params)
{
	try
	{
		$module = new WebnodeProvisioningModule();
		$project = $module->loadProject($params['serviceid']);
		$module->getWebnodeApiClient()->changePackage($project->getIdentifier(), $params['configoption1']);
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		return $e->getMessage();
	}

	return 'success';
}

/**
 * Renew an instance of a product/service.
 *
 * Attempt to renew an existing instance of a given product/service. This is
 * called any time a product/service invoice has been paid.
 *
 * @param array $params common module parameters
 *
 * @return string "success" or an error message
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_Renew(array $params)
{
	return 'success';
}

/**
 * Test connection with the given server parameters.
 *
 * Allows an admin user to verify that an API connection can be
 * successfully made with the given configuration parameters for a
 * server.
 *
 * When defined in a module, a Test Connection button will appear
 * alongside the Server Type dropdown when adding or editing an
 * existing server.
 *
 * @param array $params common module parameters
 *
 * @return array
 * @see https://developers.whmcs.com/provisioning-modules/module-parameters/
 *
 */
function webnode_TestConnection(array $params)
{
	try
	{
		// todo implement
		// Call the service's connection test function.

		$success = true;
		$errorMsg = '';
	}
	catch (Exception $e)
	{
		// Record the error in WHMCS's module log.
		logModuleCall(
			'webnode',
			__FUNCTION__,
			$params,
			$e->getMessage(),
			$e->getTraceAsString()
		);

		$success = false;
		$errorMsg = $e->getMessage();
	}

	return array(
		'success' => $success,
		'error' => $errorMsg,
	);
}

function webnode_ClientArea(array $params)
{
	$module = new WebnodeProvisioningModule();
	$projectInfo = $module->getWhmcsRepository()->getProjectInfo($params['serviceid']);
	$project = $module->getWebnodeApiClient()->findByIdentifier($projectInfo->projectIdentifier);
	if (!$module->getWebnodeApiClient()->isProjectReady($projectInfo->projectIdentifier, $project))
	{
		return [
			'templatefile' => 'templates/inactive.tpl',
			'vars' => [],
		];
	}

	if(isset($_GET['userAction']))
	{
		switch ($_GET['userAction'])
		{
			// solution copied from DUDA, it's ugly, but I can't make WHMCS to call webnode_ServiceSingleSignOn function
			case 'ssoEdit':
				header('Location: ' . $module->getWebnodeApiClient()->getSingleSignOn($projectInfo->adminUserIdentifier, $project->getPortalUrl()));
				http_response_code(302);
				die();
			case 'assignDomain':
				$module->getWebnodeApiClient()->setMasterDomainToProject($projectInfo->projectIdentifier, $params['domain']);
				$project = $module->getWebnodeApiClient()->findByIdentifier($projectInfo->projectIdentifier);
				break;
			case 'refreshCert':
				$module->getWebnodeApiClient()->refreshCertificate($params['domain']);
				$project = $module->getWebnodeApiClient()->findByIdentifier($projectInfo->projectIdentifier);
				break;
			case 'reset':
				if ($_GET['projectIdentifier'] !== $projectInfo->projectIdentifier)
				{
					throw new RuntimeException('Invalid identifier');
				}
				$project = $module->getWebnodeApiClient()->findByIdentifier($projectInfo->projectIdentifier);
				$module->getWebnodeApiClient()->resetProject($projectInfo->projectIdentifier);
				break;
			default:
				break;
		}
	}

	$domainAssigned = false;
	$httpsActive = false;

	foreach ($module->getWebnodeApiClient()->getProjectDomains($projectInfo->projectIdentifier) as $domain)
	{
		if ($domain->getDomainName() === $params['domain'])
		{
			$domainAssigned = $domain->isMasterDomain();
			$httpsActive = $domain->isHttps();
			break;
		}
	}

	return [
		'templatefile' => 'templates/active.tpl',
		'vars' => [
			'ips' => $project->getServerIpAddresses(),
			'cname' => $project->getCname(),
			'domain' => $params['domain'],
			'domainAssigned' => $domainAssigned,
			'httpsActive' => $httpsActive,
			'serviceId' => $params['serviceid'],
			'projectIdentifier' => $project->getIdentifier(),
		],
	];
}
