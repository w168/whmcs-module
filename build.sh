#!/bin/bash

rm -rf __dist

RELATIVE_ROOT_PATH=$(dirname "$0")
ROOT_DIR=$(readlink -f "$RELATIVE_ROOT_PATH")
PATH_PROVISIONING="$ROOT_DIR/__dist/modules/servers/webnode"
PATH_ADDON="$ROOT_DIR/__dist/modules/addons/webnode_conf"

mkdir -p "$PATH_ADDON"
mkdir -p "$PATH_PROVISIONING"

# copy provisioning module
cd "$ROOT_DIR/modules/servers/webnode" || exit

composer install --no-dev
composer dump -a

cp -a \
  src \
  templates \
  vendor \
  composer.json \
  logo.png \
  webnode.php \
  cron.php \
  whmcs.json \
  "$PATH_PROVISIONING"

composer install

# copy addon module
cd "$ROOT_DIR/modules/addons/webnode_conf" || exit
cp -a \
  webnode_conf.php \
  logo.png \
  whmcs.json \
  "$PATH_ADDON"

# copy translations
cd "$ROOT_DIR" || exit
cp -a overrides ./__dist
