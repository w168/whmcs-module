# WHMCS Webnode Module #


## For Users ##

* [Installing Webnode provisioning and addon modules](#installing-webnode-provisioning-and-addon-modules)
* [Configuring WHMCS using Webnode addon module](#configuring-whmcs-using-webnode-addon-module)
* [Running cron](#running-cron-optional)
* [Provisioning Webnode projects](#provisioning-webnode-projects)
* [Client area](#client-area-)
* [FAQ](#faq-)

### Installing Webnode provisioning and addon modules

1. Go to https://gitlab.com/w168/whmcs-module/-/releases
2. Download "Released ZIP file" from Assets->other
3. Copy `webnode_conf` folder from `sitebuilder-x.x.x/modules/addons` to `your_whmcs/modules/addons`
4. Copy `webnode` folder from `sitebuilder-x.x.x/modules/servers` to `your_whmcs/modules/servers`
5. Append content of the files in `sitebuilder-x.x.x/overrides` to corresponding files in `your_whmcs/lang/overrides` (i.e. append content of our `english.php` to your `english.php` and so on)
   * if you don't have `your_whmcs/lang/overrides` folder, you can just create it. More on https://developers.whmcs.com/languages/overrides/


### Configuring WHMCS using Webnode addon module

Prerequisites:
* [installed Webnode addon & provisioning modules](#installing-webnode-provisioning-and-addon-modules)
* partner configuration obtained from Webnode

In order for Webnode provisioning module to function properly it is necessary to configure the WHMCS first, using the Webnode addon module. To configure the system, go to `System Settings` > `Addon Modules` and click Configure button next to "Webnode Sitebuilder Configuration" module.

![System settings popup](/img/system-settings-popup.jpg){width=40%}

![System settings addon modules](/img/system-settings-addon-modules.jpg){width=50%}

![Webnode addon module configuration](/img/addon-configuration.jpg){width=60%}

Fill out the necessary fields with your partner information obtained from Webnode (configuration example bellow is for production, where Client ID and Secret will be provided by Webnode per partner).
* OAuth Url: https://oauth2.webnode.com/
* Api Url: https://api-gateway.webnode.com/v2/
* Client Id: [...]
* Client Secret: [...]

Save changes when done and click green `Activate` button.


### Running cron

> 💡 This step is currently optional but may be necessary in the future.

Set up a cron job which will run the `cron.php` script frequently enough so that any event produced by project creation could be consumed within a minute. Script execution example:
```shell
php cron.php "client_id" "client_secret" "oAuth2Url" "apiGatewayUrl" "count of events to consume from queue per call"
```


### Provisioning Webnode projects

After successful configuration it's possible to create a service which later can be assigned to customer orders. When creating a product in the tab `Module Settings` select the `Webnode Sitebuilder` module and a package which will be assigned to the project provisioned using the service.

![Service module settings](/img/service-module-settings.jpg){width=60%}

When service is provisioned there are several actions you can perform on a project using the buttons on the service page.

![Client order editing](/img/client-order-editing.jpg){width=60%}

* `Suspend` action will suspend the project, resulting in inability to edit the website through CMS. Frontend of the project will be still running.
* `Unsuspend` action will unblock the project pages.
* `Terminate` action will delete the project. The project deleted this way can be restored during 30 days afterward.
* `Change Package` action will change package on the project to a package set up in the currently selected `Product/Service` service.


### Client area ###

A client has access to the client area, which provides information about project status and domains. There are also buttons such as “Project page”, which will redirect the client to the project frontend and “Edit”, which will perform SSO login and redirect the logged-in user to the CMS of the project.


### FAQ ###
1. How do I login as user to Webnode Sitebuilder?
   1. Login as user in WHMCS
   2. Select service
   3. Click on edit button.


# For Developers #


## How to release ##

1. Create tag
2. Run pipeline with variable RELEASE_VERSION=x.x.x (x.x.x = your release version)
3. After tests are finished start pre-release-upload job
4. Create Release for created tag with asset with link:
   1. URL: printed by pre-release-upload job
   2. Link title: "Released ZIP file"
   3. Type: Other


## Tests ##

1. copy `tests/integration/.example.env` to `tests/integration/.env` and fill in public+secret key
2. composer install 
3. composer test-integration

