<?php
# Webnode Sitebuilder overrides
$_LANG['sitebuilder']['name'] = 'Sitebuilder';
$_LANG['sitebuilder']['ipName'] = 'Indirizzo IP del progetto';
$_LANG['sitebuilder']['header'] = 'Configurazione richiesta';
$_LANG['sitebuilder']['table']['type'] = 'Tipo';
$_LANG['sitebuilder']['table']['name'] = 'Nome';
$_LANG['sitebuilder']['table']['value'] = 'Valore';
$_LANG['sitebuilder']['domainNotAssignedText'] = 'Il dominio non è ancora assegnato al tuo progetto. Imposta i record DNS sui valori mostrati sopra e assegna il dominio.';
$_LANG['sitebuilder']['domainNotAssignedButton'] = 'Assegnare un dominio';
$_LANG['sitebuilder']['httpsNotActiveText'] = 'Il dominio non ha HTTPS attivo. Configurare i registri DNS con i valori che verranno visualizzati e aggiorna il certificato.';
$_LANG['sitebuilder']['httpsNotActiveButton'] = 'Aggiorna il certificato';
$_LANG['sitebuilder']['cmsHeader'] = 'Modifica in Sitebuilder:';
$_LANG['sitebuilder']['cmsButton'] = 'Modifica';
$_LANG['sitebuilder']['inactiveText'] = 'Il tuo progetto è in preparazione, aggiornalo più tardi.';
$_LANG['sitebuilder']['inactiveButton'] = 'Aggirna';
