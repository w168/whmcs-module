<?php
# Webnode Sitebuilder overrides
$_LANG['sitebuilder']['name'] = 'Sitebuilder';
$_LANG['sitebuilder']['ipName'] = 'IP del proyecto';
$_LANG['sitebuilder']['header'] = 'Configuración requerida';
$_LANG['sitebuilder']['table']['type'] = 'Tipo';
$_LANG['sitebuilder']['table']['name'] = 'Nombre';
$_LANG['sitebuilder']['table']['value'] = 'Valor';
$_LANG['sitebuilder']['domainNotAssignedText'] = 'El dominio aún no está asignado a su proyecto. Configure los registros DNS con los valores que se muestran arriba y asigne el dominio.';
$_LANG['sitebuilder']['domainNotAssignedButton'] = 'Asignar dominio';
$_LANG['sitebuilder']['httpsNotActiveText'] = 'El dominio no tiene HTTPS activo. Configure los registros DNS con los valores que se muestran arriba y actualice el certificado.';
$_LANG['sitebuilder']['httpsNotActiveButton'] = 'Actualizar certificado';
$_LANG['sitebuilder']['cmsHeader'] = 'Editar en Sitebuilder:';
$_LANG['sitebuilder']['cmsButton'] = 'Editar';
$_LANG['sitebuilder']['inactiveText'] = 'Su proyecto se está preparando, por favor actualícelo más tarde.';
$_LANG['sitebuilder']['inactiveButton'] = 'Actualizar';
