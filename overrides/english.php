<?php
# Webnode Sitebuilder overrides
$_LANG['sitebuilder']['name'] = 'Sitebuilder';
$_LANG['sitebuilder']['ipName'] = 'Project IP adresses';
$_LANG['sitebuilder']['header'] = 'Required Configuration';
$_LANG['sitebuilder']['table']['type'] = 'Type';
$_LANG['sitebuilder']['table']['name'] = 'Name';
$_LANG['sitebuilder']['table']['value'] = 'Value';
$_LANG['sitebuilder']['domainNotAssignedText'] = 'The domain is not assigned to your project yet. Please setup the DNS records to the values shown above and assign the domain.';
$_LANG['sitebuilder']['domainNotAssignedButton'] = 'Assign Domain';
$_LANG['sitebuilder']['httpsNotActiveText'] = 'The domain does not have not active HTTPS. Please setup the DNS records to the values shown above and refresh the certificate.';
$_LANG['sitebuilder']['httpsNotActiveButton'] = 'Refresh certificate';
$_LANG['sitebuilder']['cmsHeader'] = 'Edit in Sitebuilder:';
$_LANG['sitebuilder']['cmsButton'] = 'Edit';
$_LANG['sitebuilder']['inactiveText'] = 'Your project is being prepared, please refresh it later.';
$_LANG['sitebuilder']['inactiveButton'] = 'Refresh';
