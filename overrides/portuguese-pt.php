<?php
# Webnode Sitebuilder overrides
$_LANG['sitebuilder']['name'] = 'Sitebuilder';
$_LANG['sitebuilder']['ipName'] = 'Endereços IP do projeto';
$_LANG['sitebuilder']['header'] = 'Configuração necessária';
$_LANG['sitebuilder']['table']['type'] = 'Tipo';
$_LANG['sitebuilder']['table']['name'] = 'Nome';
$_LANG['sitebuilder']['table']['value'] = 'Valor';
$_LANG['sitebuilder']['domainNotAssignedText'] = 'O domínio ainda não está atribuído ao seu projeto. Configure os registos DNS com os valores apresentados acima e atribua o domínio.';
$_LANG['sitebuilder']['domainNotAssignedButton'] = 'Atribuir domínio';
$_LANG['sitebuilder']['httpsNotActiveText'] = 'O domínio não tem HTTPS ativo. Configure os registos DNS para os valores apresentados acima e atualize o certificado.';
$_LANG['sitebuilder']['httpsNotActiveButton'] = 'Atualizar certificado';
$_LANG['sitebuilder']['cmsHeader'] = 'Edite no Sitebuilder:';
$_LANG['sitebuilder']['cmsButton'] = 'Editar';
$_LANG['sitebuilder']['inactiveText'] = 'O seu projeto está a ser preparado, por favor atualize-o mais tarde.';
$_LANG['sitebuilder']['inactiveButton'] = 'Atualizar';
