<?php
# Webnode Sitebuilder overrides
$_LANG['sitebuilder']['name'] = 'Sitebuilder';
$_LANG['sitebuilder']['ipName'] = 'Projektin IP-osoite';
$_LANG['sitebuilder']['header'] = 'Vaadittu konfiguraatio';
$_LANG['sitebuilder']['table']['type'] = 'Tyyppi';
$_LANG['sitebuilder']['table']['name'] = 'Nimi';
$_LANG['sitebuilder']['table']['value'] = 'Arvo';
$_LANG['sitebuilder']['domainNotAssignedText'] = 'Verkkotunnusta ei ole vielä määritetty projektillesi. Ole hyvä ja aseta DNS-tietueet yllä esitettyihin arvoihin ja ohjaa verkkotunnus.';
$_LANG['sitebuilder']['domainNotAssignedButton'] = 'Ohjaa verkkotunnus';
$_LANG['sitebuilder']['httpsNotActiveText'] = 'Verkkotunnuksella ei ole aktiivista HTTPS:varmennetta. Määritä DNS-tietueet yllä esitettyihin arvoihin ja päivitä varmenne.';
$_LANG['sitebuilder']['httpsNotActiveButton'] = 'Päivitä varmenne';
$_LANG['sitebuilder']['cmsHeader'] = 'Muokkaa Sitebuilderissa:';
$_LANG['sitebuilder']['cmsButton'] = 'Muokkaa';
$_LANG['sitebuilder']['inactiveText'] = 'Projektia valmistellaan parhaillaan, ole hyvä ja päivitä se myöhemmin.';
$_LANG['sitebuilder']['inactiveButton'] = 'Päivitä';
